//load modules

//declare internals
var internals = {};


exports.register = function(server,options,next){

    server.route({
        method:'GET',
        path:'/users/ok',
        handler:function(request,reply){
            reply({status:'ok'});
        }
    })

    server.route({
        method:'GET',
        path:'/users/not-ok',
        handler:function(request,reply){
            reply({status:'not-ok'});
        }
    })

    next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};